// SendHexData.cpp : 实现文件
//

#include "stdafx.h"
#include "TxPrinterTest.h"
#include "SendHexData.h"
#include "afxdialogex.h"
#include   <afxpriv.h>
#include <vector>
#include "export.h"

// CSendHexData 对话框

IMPLEMENT_DYNAMIC(CSendHexData, CDialogEx)

CSendHexData::CSendHexData(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSendHexData::IDD, pParent)
	, HexData(NULL)
{

	EnableAutomation();
//	GetDlgItem (IDC_EDIT1)->SetFocus();

}

CSendHexData::~CSendHexData()
{
}

void CSendHexData::OnFinalRelease()
{
	// 释放了对自动化对象的最后一个引用后，将调用
	// OnFinalRelease。基类将自动
	// 删除该对象。在调用该基类之前，请添加您的
	// 对象所需的附加清理代码。

	CDialogEx::OnFinalRelease();
}

void CSendHexData::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSendHexData, CDialogEx)
	ON_BN_CLICKED(IDCANCEL, &CSendHexData::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CSendHexData::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON2, &CSendHexData::OnBnClickedButton2)
//	ON_EN_CHANGE(IDC_EDIT1, &CSendHexData::OnEnChangeEdit1)
	ON_BN_CLICKED(IDC_BUTTON1, &CSendHexData::OnBnClickedButton1)
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CSendHexData, CDialogEx)
END_DISPATCH_MAP()

// 注意: 我们添加 IID_ISendHexData 支持
//  以支持来自 VBA 的类型安全绑定。此 IID 必须同附加到 .IDL 文件中的
//  调度接口的 GUID 匹配。

// {5EDFE126-28C1-4E0E-9E75-ACC6BFFDB824}
static const IID IID_ISendHexData =
{ 0x5EDFE126, 0x28C1, 0x4E0E, { 0x9E, 0x75, 0xAC, 0xC6, 0xBF, 0xFD, 0xB8, 0x24 } };

BEGIN_INTERFACE_MAP(CSendHexData, CDialogEx)
	INTERFACE_PART(CSendHexData, IID_ISendHexData, Dispatch)
END_INTERFACE_MAP()
//BOOL CSendHexData::OnInitDialog()
//{
//	CDialogEx::OnInitDialog();
//	((CEdit*)GetDlgItem(IDC_EDIT1))->SetFocus();
//	return TRUE;
//}


// CSendHexData 消息处理程序


void CSendHexData::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnCancel();
}


void CSendHexData::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnOK();
}


void CSendHexData::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	((CEdit*)GetDlgItem(IDC_EDIT100))->SetWindowTextW(_T(""));
}


void CSendHexData::OnEnChangeEdit1()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码

}


void CSendHexData::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	CString Cbuf;
//	int Idx,Label,end;
//	char temp[2];
//	Label=Idx=1;
//	end=0;
	((CEdit*)GetDlgItem(IDC_EDIT100))->GetWindowText(Cbuf);
	USES_CONVERSION;
	char* tbuf=T2A(Cbuf);
	if (strlen(tbuf)==0) {return;}
//	char* t2buf=new char[(strlen(tbuf)/2)+1];
//	char *stopstring;
/*
	for (unsigned count=0;count<=(strlen(tbuf)-1);count=count+1)
	{
		if ((tbuf[count]!='\r')&&(tbuf[count]!='\n')&&(tbuf[count]!=' ')&&(tbuf[count]!='\t')&&(tbuf[count]!='\v'))
		{
			if (Label==1)
			{
				temp[0]=tbuf[count];
				Label=Label+1;
			}
			else
			{
				temp[1]=tbuf[count];
				Label=1;
				t2buf[Idx]=int(strtol(temp,&stopstring,16));
//				buf[Idx]=strtol("35",&stopstring,16);
//				char b=strtol(temp,&stopstring,16);
				Idx=Idx+1;
				end=end+1;
			}
		}
	}
	if (Label==2) {AfxMessageBox(IDS_STRING111); return;}
//	t2buf[end+1]='\0';
//	char *buf=new char[end+1];//!!!!
	char* buf=new char[3];
	for (Idx=1;Idx<=end;Idx=Idx+1)
	{
		buf[Idx]=t2buf[Idx];
	}
	delete []t2buf;
*/


//	int Idx,Label,end;
	int Label=1;
	char temp[2];
//	Label=Idx=1;
//	end=0;
	char *stopstring;
	using std::vector;
	vector<char> buf;
	for (unsigned count=0;count<=(strlen(tbuf));count=count+1)
	{
		if ((tbuf[count]!='\r')&&(tbuf[count]!='\n')&&(tbuf[count]!=' ')&&(tbuf[count]!='\t')&&(tbuf[count]!='\v')&&(tbuf[count]!='\0'))
		{
			if (Label==1)
			{
				temp[0]=tbuf[count];
				Label=Label+1;
			}
			else
			{
				temp[1]=tbuf[count];
				Label=1;
				buf.push_back(int(strtol(temp,&stopstring,16)));
				//buf[Idx]=int(strtol(temp,&stopstring,16));
//				buf[Idx]=strtol("35",&stopstring,16);
//				char b=strtol(temp,&stopstring,16);
//				Idx=Idx+1;
				//end=end+1;
			}
		}
	}
	if (Label==2) {AfxMessageBox(IDS_STRING111); return;}
 //   BOOL a=TxWritePrinter(&buf[0],buf.size());
    if (TxWritePrinter(&buf[0],buf.size())<=0) {AfxMessageBox(IDS_STRING111);}
	//	char *out = new char[buf.size()+1];
//	copy(buf.begin(),buf.end(),out);
//	out[buf.size]='/0';
//	TxWritePrinter(out, strlen(out));
//	t2buf[end+1]='\0';
//	char *buf=new char[end+1];//!!!!
//	char* buf=new char[3];
//	for (Idx=1;Idx<=end;Idx=Idx+1)
//	{
//		buf[Idx]=t2buf[Idx];
//	}
//	delete []t2buf;
}
