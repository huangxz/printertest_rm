// DenDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "DenDlg.h"
#include "afxdialogex.h"
#include "TxPrinterTest.h"
#include "export.h"
#include <vector>

// CDenDlg 对话框

IMPLEMENT_DYNAMIC(CDenDlg, CDialogEx)

CDenDlg::CDenDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDenDlg::IDD, pParent)
{

	EnableAutomation();

}

CDenDlg::~CDenDlg()
{
}

void CDenDlg::OnFinalRelease()
{
	// 释放了对自动化对象的最后一个引用后，将调用
	// OnFinalRelease。基类将自动
	// 删除该对象。在调用该基类之前，请添加您的
	// 对象所需的附加清理代码。

	CDialogEx::OnFinalRelease();
}

void CDenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BOOL CDenDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	((CComboBox*)GetDlgItem(IDC_COMBO200))->SetCurSel(0);
	return TRUE;
}
BEGIN_MESSAGE_MAP(CDenDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CDenDlg::OnBnClickedOk)
//	ON_WM_CREATE()
	ON_CBN_SELCHANGE(IDC_COMBO200, &CDenDlg::OnCbnSelchangeCombo200)
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CDenDlg, CDialogEx)
END_DISPATCH_MAP()

// 注意: 我们添加 IID_IDenDlg 支持
//  以支持来自 VBA 的类型安全绑定。此 IID 必须同附加到 .IDL 文件中的
//  调度接口的 GUID 匹配。

// {87C07937-4C2C-4C47-9145-C46DB7D4FF18}
static const IID IID_IDenDlg =
{ 0x87C07937, 0x4C2C, 0x4C47, { 0x91, 0x45, 0xC4, 0x6D, 0xB7, 0xD4, 0xFF, 0x18 } };

BEGIN_INTERFACE_MAP(CDenDlg, CDialogEx)
	INTERFACE_PART(CDenDlg, IID_IDenDlg, Dispatch)
END_INTERFACE_MAP()


// CDenDlg 消息处理程序


void CDenDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
   	CString temp;
	using std::vector;
	vector<char> Out(10);
	Out[0]=0x0;
	Out[1]=0x1b;
	Out[2]=0x11;
	Out[3]=0x1;
	Out[4]=0x2;
	Out[5]=0x3;
	Out[6]=0x4;
	Out[7]=0x5;
	Out[8]=0x6;
	((CComboBox*)GetDlgItem(IDC_COMBO200))->GetWindowText(temp);
	if (temp=="普通") {Out[9]=0x0;}
	if (temp=="略深") {Out[9]=0x1;}
	if (temp=="深") {Out[9]=0x2;}
	if (temp=="较深") {Out[9]=0x3;}
    if (temp=="略浅") {Out[9]=0x4;}
	TxWritePrinter(&Out[0],Out.size());
	CDialogEx::OnOK();
}


//int CDenDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
//{
//	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
//		return -1;
//
//	// TODO:  在此添加您专用的创建代码
//	((CComboBox*)GetDlgItem(IDC_COMBO200))->SetCurSel(0);
//	return 0;
//}


void CDenDlg::OnCbnSelchangeCombo200()
{
	// TODO: 在此添加控件通知处理程序代码
}
