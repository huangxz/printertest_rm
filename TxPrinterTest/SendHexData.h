#pragma once


// CSendHexData 对话框

class CSendHexData : public CDialogEx
{
	DECLARE_DYNAMIC(CSendHexData)

public:
	CSendHexData(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CSendHexData();
//	virtual BOOL OnInitDialog();
	virtual void OnFinalRelease();

// 对话框数据
	enum { IDD = IDD_SENDHEXDATA };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
public:
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnEnChangeEdit1();
	char* HexData;
	afx_msg void OnBnClickedButton1();
};
